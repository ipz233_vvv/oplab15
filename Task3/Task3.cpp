#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*
�������� 3. �������� ���������� ����������� ������ � ����������
����������� (�� ��������� ����������� ������������ ������):

������ ����������: ������, ���������
��� �������� ������: �����
������� ����������: �� ���������
*/
int main() {
	srand(time(NULL));
	int n, k;
	float m[100][100];
	float m2[10000];
	printf("Enter matrix size n and k (both less than 100):\n");
	scanf("%d", &n);
	scanf("%d", &k);

	for (int d = 0; d < 2; d++) {
		printf("\nGenerating new random matrix...\n");
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < k; j++) {
				m[i][j] = (rand() % 200 - 99 )/10.;
				m2[i * k + j] = m[i][j];
				printf("% 2.1f ", m[i][j]);
			}
			printf("\n");
		}
		if (d == 0) {
			printf("Sorting using bubble sort:\n");
			for (int j = 0; j < n * k; j++) {
				bool swap = false;
				for (int l = 0; l < n * k - 1; l++) {
					if (m2[l] < m2[l + 1])
					{
						int t = m2[l];
						m2[l] = m2[l + 1];
						m2[l + 1] = t;
						swap = true;
					}
				}
				if (!swap)
					break;
			}
		}
		else {
			printf("Sorting using insertion sort:\n");
			for (int i = 1; i < n*k; i++)
			{
				int t = m2[i];
				for (int j = i-1; j >= 0; j--)
				{
					if (m2[j] > t)
						break;
					m2[j + 1] = m2	[j];
					m2[j] = t;
				}
			}

		}

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < k; j++) {
				m[i][j] = m2[i * k + j];
				printf("% 2.1f ", m[i][j]);
			}
			printf("\n");
		}
	}
	return 0;
}