#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*
�������� 1. ���� ��������� �������
��������� �������� ������ ����� � ������� ���������.
*/
int main() {
	srand(time(NULL));
	int n;
	int m[100][100];
	printf("Enter matrix size n (less than 100):\n");
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			m[i][j] = rand() % n;
			printf("%02d ", m[i][j]);
		}
		printf("\n");
	}
	for (int i = 0; i < n; i++) {
		if(i%2==1)
		for (int j = 0; j < n; j++) {
			bool swap = false;
			for (int k = 1; k < n; k++) {
				if (m[i][k] < m[i][k - 1])
				{
					int t = m[i][k];
					m[i][k] = m[i][k - 1];
					m[i][k - 1] = t;
					swap = true;
				}
			}
			if (!swap)
				break;
		}
	}
	printf("\n");
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			printf("%02d ", m[i][j]);
		}
		printf("\n");
	}
	return 0;
}